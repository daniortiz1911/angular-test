import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {data} from './fixtures';
import { BonusComponent } from './bonus.component';

describe('BonusComponent', () => {
  let component: BonusComponent;
  let fixture: ComponentFixture<BonusComponent>;
  let info: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonusComponent ]
    })
    .compileComponents();
    info = data;
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.loadData()).toEqual(info);
  });
});
