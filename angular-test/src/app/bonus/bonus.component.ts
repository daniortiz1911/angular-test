import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.scss']
})
export class BonusComponent implements OnInit {

  public data: any;
  constructor(private configServ: ConfigService,
    private router: Router) {

  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    return this.configServ.getData().subscribe((data: {}) => {
      this.data = data[0];
      // console.log(this.data);
    })
  }

  send() {
    this.router.navigate(['/bonus/alert',
      {
        amount: this.data.amount,
        prize: this.data.prize,
        game: this.data.game,
        countdown: this.data.countdown,
      }]);
  }

}
