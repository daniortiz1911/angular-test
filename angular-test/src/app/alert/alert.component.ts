import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  public amount: string;
  public countdown: string;
  public prize: string;
  public game: string;
  constructor(private route: ActivatedRoute, ) {
    this.amount = this.route.snapshot.params.amount;
    this.countdown = this.route.snapshot.params.countdown;
    this.prize = this.route.snapshot.params.prize;
    this.game = this.route.snapshot.params.game;
  }

  ngOnInit() {
  }

}
