import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BonusComponent } from './bonus/bonus.component';
import { AlertComponent } from './alert/alert.component';

const routes: Routes = [
  {
    path: 'bonus',
    component: BonusComponent
  },
  {
    path: 'bonus/alert',
    component: AlertComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
